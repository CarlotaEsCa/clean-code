package case3;


public class Calculator {
    
    public long sum(int min, int max){
        return calculate(min,max,new Operation(){

            @Override
            public long calculate(long value) {
                return value;
            }
        });
    }
    
    public long sumOfSquares(int min, int max){
        return calculate(min,max,new Operation(){

            @Override
            public long calculate(long value) {
                return value*value;
            }
        });
    }
    
    public long calculate(int min, int max, final Operation closure){
        long result = 0;
        for (int i = min ; i <= max ; i++)
            result += closure.calculate(i);
	return result;
    }
}

