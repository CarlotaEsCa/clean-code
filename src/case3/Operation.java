package case3;

public interface Operation {
    public long calculate(long value);
}
